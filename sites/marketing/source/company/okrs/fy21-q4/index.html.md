---
layout: markdown_page
title: "FY21-Q4 OKRs"
description: "View GitLabs Objective-Key Results for FY21 Q4. Learn more here!"
canonical_path: "/company/okrs/fy21-q4/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from November 1, 2020 to January 31, 2021.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2020-09-28 | CEO shares top goals with E-group for feedback |
| -4 | 2020-10-05 | CEO pushes top goals to this page |
| -3 | 2020-10-12 | E-group pushes updates to this page |
| -2 | 2020-10-19 | E-group 50 minute draft review meeting |
| -2 | 2020-10-19 | E-group discusses with teams |
| -1 | 2020-10-26 | CEO reports give a How to Achieve presentation. Pre-Meeting and How to Achieve Presentation recordings are added to this page by EBAs |
| 0  | 2020-11-02 | CoS updates OKR page for current quarter to be active |
| +5 | 2020-12-08 | Review previous and next quarter during the next Board meeting |

## OKRs

### 1. CEO: Grow Incremental ACV (IACV)
[Epic 1010](https://gitlab.com/groups/gitlab-com/-/epics/1010)
1. **CEO KR:** Complete 2 pricing and packaging changes supported by data
1. **CEO KR:** All X pain points in fullfillment are addressed and Zuora is the SSoT for subscriptions
1. **CEO KR:** Improve growth efficiency to X so we are on track to reach 1.0 in FY22 

### 2. CEO: Popular next generation product
[Epic 1011](https://gitlab.com/groups/gitlab-com/-/epics/1011)
1. **CEO KR:** Improve user experience => SUS score over X. Identify and fix X transient bugs. LCP under 2.5 seconds for 75th slowest percentile on 70 measured pages 
1. **CEO KR:** Increase conversion rate by getting X% of active users on EE, get X in MRARR, and have Predicted GMAU in a dashboard for all X of the DevOps groups.
1. **CEO KR:** Ship the [5 minute production app](https://docs.google.com/document/d/1xp0Ax5_svn8uwB75pM_4tDp4fHcPHB6pSFDWWUym2a4/edit), have a video that takes less than 5 minutes, and get 100,000 impressions for blog posts about it.

### 3. CEO: Great team
[Epic 1012](https://gitlab.com/groups/gitlab-com/-/epics/1012)
1. **CEO KR:** All X courses across the company are in on L&D system
1. **CEO KR:** Complete 10,000 certifications across GitLab team members and the wider community
1. **CEO KR:** Transition to performance based compensation increases 

## Pre-Meeting Recordings

* Engineering
* Finance
* Legal
* Marketing
* People
* Product
* Sales

## How to Achieve Presentations

* Engineering
* Finance
* Legal
* Marketing
* People
* Product
* Sales
