---
layout: handbook-page-toc
title: "Application Security Stable Counterparts"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Stable Counterparts

The overall goal of application security stable counterparts is to help
integrate security themes early in the development process. This is with
the goal of reducing the number of vulnerabilities released over the long
term.

### Technical Goals
- Assist in threat modeling features to identify areas of risk prior to
  implementation
- Code and app sec reviews
- Provide guidance on security best practices
- Improve security testing coverage
- Assistance in prioritizing security fixes

### Non-technical Goals
- Enable development team to self-identify security risk early
- Help document and solve pain points when it comes to security process
- Identify vulnerability areas to target for training and/or automation
- Assist in cross-team communication of security related topics
